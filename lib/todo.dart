//  import 'dart:html';

// import 'package:flutter/cupertino.dart';
// import 'package:flutter/src/widgets/container.dart';
// import 'package:flutter/src/widgets/framework.dart';

// class TodoLIST extends StatelessWidget {
//   const TodoLIST({super.key});

//   @override
//   Widget build(BuildContext context) {
//     return Container(
//       child: Column(
//         children: [
//           Text("ici il y'a la todo")
//         ],
//       )
//     );
//   }
// }
import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/container.dart';
import 'package:flutter/src/widgets/framework.dart';

import 'contact.dart';

class TodoLIST extends StatefulWidget {
  const TodoLIST({super.key});

  @override
  State<TodoLIST> createState() => _TodoLISTState();
}

class _TodoLISTState extends State<TodoLIST> {
  int selectedIndex = -1;
  List<Contact> contacts =List.empty(growable: true);
  TextEditingController controllerArtiste = TextEditingController();
  TextEditingController controllerTitre = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('To do List'),
        centerTitle: true,
     
      ),
      body: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          children: [
            const SizedBox(height: 10,),
             TextField(
              controller:controllerArtiste,
                decoration: InputDecoration(
                  hintText: 'Nom de l\' artiste',
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.all(Radius.circular(10)),
                  )
                ),
            ),
             const SizedBox(height: 10,),
             TextField(
              controller:controllerTitre,
                decoration: InputDecoration(
                  hintText: "Titre du song",
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.all(Radius.circular(10)),
                  )
                ),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children:[
                ElevatedButton(
                  
                  onPressed: (){
                    String titre = controllerArtiste.text.trim();
                    String song = controllerTitre.text.trim();


                    if(titre.isNotEmpty && song.isNotEmpty){
                      setState(() {
                          //
                          contacts.add(Contact(titre: titre,song: song));
                      });
                    }
                  }, 
                  child:const Text("Enregistrer")
                  ),
                const   SizedBox(width: 10,height: 30,),
                ElevatedButton(
                  onPressed: (){
                       String titre = controllerArtiste.text.trim();
                    String song = controllerTitre.text.trim();
                    if(titre.isNotEmpty && song.isNotEmpty){
                      setState(() {
                        controllerArtiste.text = '';
                        controllerArtiste.text = '';
                        contacts[selectedIndex].titre = titre;
                        contacts[selectedIndex].song = song;
                        selectedIndex =-1;

                      });
                    }
                  }, 
                  child:const Text("Modifier")
                  ),
              ],
            ),
            contacts.isEmpty
            ? 
            const Text(
              "Pas encore d'element dans la liste...",
              style:TextStyle(fontSize: 22)
              ):
            Expanded(
              child: ListView.builder(
                itemCount: contacts.length,
                itemBuilder: ((context, index)=>getRow(index)),
                  // cree une methode getRow
                 
                ),
            ),
              
            
          ],
        ),
      ),
    );
  }
  // methode getRow
 getRow(int index){
    return  Card(
      child: ListTile(
        leading: CircleAvatar(
          backgroundColor: index%2==0 ?Colors.deepPurple : Colors.purple,
          foregroundColor: Colors.white24,
          child:Text(contacts[index].titre[0]),

        ),
                    title: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(contacts[index].titre,style: TextStyle(fontWeight: FontWeight.bold),),
                       Text(contacts[index].song),
                      ],
                    ),
                    trailing: SizedBox(
                      width: 70,
                      child: Row(
                        children: [
                       InkWell(
                        onTap: ((){
                          //update
                          controllerArtiste.text = contacts[index].titre;
                          controllerArtiste.text = contacts[index].song;

                        }),
                        child: const   Icon(Icons.edit)),
                         InkWell(
                          onTap: (() {
                              setState(() {
                                contacts.remove(index);
                              });
                          }),

                          child: const  Icon(Icons.delete)),
                        ],
                      ),
                    ),
                  ),
    );
 }
}


